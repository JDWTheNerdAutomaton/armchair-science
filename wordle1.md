# Sunday afternoon bad data science: Some bad suggestions for how to start solving your daily wordle - 2022-02 JDW

## Abstract: 
After todays daily wordle I made some armchair data science, to get an idea, which words might give the best information to start the game. With some statistics I propose some words that are as good of a start as any, and then will explain why they are not.

## Introduction:
In this paper I propose some words to start your daily wordle. I guess it already has all been discussed on some twitter bubble, but I am honestly to lazy to google. 

## Method:
I downloaded the first best list of English words that I could find [1]. 
Afterwards I filtered the list for the 5 letter words and split the remaining words into multiple columns in excel (e.g.): 
"words w o r d s" . 
fig1: an example row of data

### Calculation 1:
I ran some basic frequency calculations to calculate 2 statistics that seemed useful: 
 - For each column which is the most frequent letter
 - which are the most frequent characters overall.  
Out of those statistics I calculated 2 scores for each word: The Simple Exact Position Score and the Simple Total Score. 
The Simple Exact Position Score is defined as the sum of the frequencies for each letter in the word at the specific position. 
The Simple Total Score is defined as the sum of overall frequencies of each letter in the word regardless of their position.

### Calculation 2:
To put those statistics into context I ran calculations on the frequency of letter pairs occuring directly together and a second calculation of letter pairs which were occuring together, possibly with other letters in between. 
I then constructed the best word, by starting with the most frequent letter in the first position and then chaining letters of the highest frequency from the pairing, ensuring a real word comes out. 

## Results:
The "word" created by using the most frequent characters per position is not very helpful: saaes
The total most frequent letters overall in the list were in order of frequency: searo
The highest total score should help us analyzing a big weakness of this study right away: asses
Filtering duplicate letters, the best words for total score are:
1. arose
2. laser / earls / reals
3. tears / stare / aster

This method does not discriminate for positioning, hence, multiple words share a place
The Top 3 best words by total score by position are again revealing another weakness of the current method, the plural forms: 
1. cares
2. pares 
3. cores

The first non plural non verbform word by this method is (in position 961!): saver
The first three words constructed by the letter pair method is (along with their total score position): 
1. steal (113)
2. steam (331)
3. steak (433)

## Discussion:
It is kinda obvious that I need to filter the data for at least plurals (e.g. auras), but its a bit harder to do that, since some words seem to belong into the list like "abyss". Also the verb forms simple past (acted), and 3rd person singular (bails), are part of the list but maybe simple past forms are used in the game, which is a research question I will tackle in a follow up project. What is clear though, is that the presence of plurals skews the data towards favoring "s", which is problematic. [2] describes that the game should be based on around 2500 words and the filtered list had 3700, out of which 1200 ended with "s". So I sense a pattern.
If you want to start with a word from the study, I suggest to use "laser", mainly because lasers are cool. 

## References
[1] http://www.mieliestronk.com/corncob_lowercase.txt
[2] https://www.zeit.de/.../online-wortraetsel-wordle-spiel...
